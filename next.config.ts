import {NextConfig} from "next";

const nextConfig: NextConfig = {
  experimental: {
    typedRoutes: false,
  },
  rewrites: async () => ([{
    source: '/sites/:path*',
    destination: 'https://eb12ccf9-ce7e-48b4-97b0-fc77ac31af1d.selstorage.ru/pet4u/sites/:path*'
  }]),
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {hostname: 'eb12ccf9-ce7e-48b4-97b0-fc77ac31af1d.selstorage.ru', protocol: 'https', pathname: '/pet4u/**'}
    ]
  },
}

module.exports = nextConfig
