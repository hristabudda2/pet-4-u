/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./features/**/*.{js,ts,jsx,tsx,mdx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      keyframes: {
        'slide-down': {
          '0%': {top: '-100%'},
          '100%': {top: '0px'},
        },
        'slide-up': {
          '0%': {top: '0px'},
          '100%': {top: '-100%'},
        }
      },

      animation: {
        'slide-down': 'slide-down .5s ease-in-out',
        'slide-up': 'slide-up .5s ease-in-out'
      },
      colors: {
        'brand-blue-100': '#8793d4',
        'brand-blue-400': '#102B53',
        'brand-blue-50': '#ECF3F9',
        'brand-orange-100': '#F5732A',
        'brand-orange-50': '#FFEFE6',
        'brand-black': '#040404',
        'brand-gray-100': '#81899B',
        'brand-gray-50': '#D5E1EB',
        'brand-bg': '#F8F8F8'
      },
      fontFamily: {
        montserrat: ['var(--font-montserrat-alternates)'],
        jost: ['var(--font-jost)']
      },
      gridTemplateRows: {
        headerBlock: '0.5fr 1fr'
      },
      // screens: {
      //   sm: {min: '440px', max: '600px'},
      //   md: {min: '1024px'},
      // },
    },
  },
  plugins: [require('@tailwindcss/typography'),],
}
