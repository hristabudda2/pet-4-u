import {Actor} from "@features/01-init/types";


export function useActorTexts<T extends {[key in Actor]: Record<any, any>}>({actor, dictionary}: {dictionary: T, actor: Actor}): T['buyer'] {
    return dictionary[actor]
}