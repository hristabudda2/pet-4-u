import {Actor} from "@features/01-init/types";

type UseAccentColorHook = (payload: {actor: Actor, inversed?: boolean}) => ({text: string, border: string, bg: string})

export const useAccentColor: UseAccentColorHook = ({actor, inversed = false}) => {
    const textColorDict: {[key in Actor]: string} = inversed? {buyer: 'text-brand-orange-100', seller: 'text-brand-blue-100'}: {buyer: 'text-brand-blue-100', seller: 'text-brand-orange-100'};
    const borderColorDict: {[key in Actor]: string} = inversed? {buyer: 'border-brand-orange-100', seller: 'border-brand-blue-100'}: {buyer: 'border-brand-blue-100', seller: 'border-brand-orange-100'};
    const bgColorDict: {[key in Actor]: string} = inversed? {buyer: 'bg-brand-orange-100', seller: 'bg-brand-blue-100'}: {buyer: 'bg-brand-blue-100', seller: 'bg-brand-orange-100'};
    return {text: textColorDict[actor], border: borderColorDict[actor], bg: bgColorDict[actor]}
}