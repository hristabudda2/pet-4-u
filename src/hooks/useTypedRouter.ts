import { NextRouter, useRouter } from 'next/router';
import { Spread } from '@specs/helpers';


type TypedRouter<Query> = Omit<NextRouter, 'query'> & {query: Query}

export function useTypedRouter<Query extends [...any]>(): TypedRouter<Spread<Query>>  {
    const router = useRouter();

    return router as TypedRouter<Spread<Query>>;
}