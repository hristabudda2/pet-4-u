'use client';
import React, {FC, PropsWithChildren, ReactNode, useEffect, useState} from 'react';
import Popup from '@components/Popup';

type RenderProps<T> = (props: T) => ReactNode
type PopupContentProps<T> = {
  children: RenderProps<T>
};

export type OpenPopupFn = (PopupContent: ReactNode) => void

export function usePopup<T>() {
  const [isOpen, setIsOpen] = useState(false);
  const [Content, setContent] = useState<ReactNode>();
  const open: OpenPopupFn = (PopupContent: ReactNode) => {
    setIsOpen(true)
    setContent(PopupContent)
  };
  const close = () => setIsOpen(false)
  useEffect(() => {
    if (isOpen) {
      document.body.className += ' overflow-hidden'
    } else {
      if (document.body.className.includes('overflow-hidden')) {
        document.body.className = document.body.className.replace(' overflow-hidden', '')
      }
    }
  }, [isOpen])
  return {
    Popup: ({children}: PropsWithChildren) => <Popup isOpen={isOpen} close={close}>{Content ? Content : children}</Popup>,
    open,
    isOpen
  }
}