'use client'
import {useSearchParams} from "next/navigation";
import {ActorSelector} from "@features/02-login/components/ActorSelector";
import {Nullable} from "../../../utils/types";
import {Actor} from "@features/01-init/types";


const Page = () => {

    return (<div className={'flex flex-col items-center pt-24 px-80 text-center bg-[#F8F8F8]'}>
        <h1 className={'font-montserrat font-bold text-2xl text-brand-black mb-5'}>Приветствуем на Pet4U</h1>
        <h3 className={'font-jost text-brand-black mb-12'}>Выберите роль, которая соответсвтует вашим потребностям</h3>
        <ActorSelector actor={'buyer'}/>
    </div>)
}

export default Page