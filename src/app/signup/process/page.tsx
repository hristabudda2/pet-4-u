'use client';
import {StepProgressBar} from "@features/02-login/components/StepProgressBar";
import {useMemo, useState} from "react";
import {Incrementer, Step} from "@features/02-login/types";
import {DataStep} from "@features/02-login/components/DataStep";
import {ConfirmationStep} from "@features/02-login/components/ConfirmationStep";
import {EndedStep} from "@features/02-login/components/EndedStep";


const registrationSteps: Array<Step> = [
    {description: 'Личные данные'},
    // {description: 'Подтверждение'},
    {description: 'Завершение регистрации'},
];

const Page = () => {
    const [step, setStep] = useState(0);



    const nextStep: Incrementer = () => setStep(prev => prev + 1);

    const stepDict = {
        0: <DataStep nextStep={nextStep}/>,
        // 1: <ConfirmationStep/>,
        1: <EndedStep/>
    }

    const component = useMemo(() => stepDict[step], [step])
    return <div className={'pt-32 px-60'}>
        <StepProgressBar steps={registrationSteps} currentStep={step}/>
        {component}
    </div>
}

export default Page