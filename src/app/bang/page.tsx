'use client'
import {Button} from "@components/Button";
import {downloadArticleImages} from "@features/04-community/data/downloadArticleImages";
import {putObject} from "@features/04-community/data/uploadArticleImagesToS3";
import {cleanArticleDuplicates} from "@features/04-community/data/cleanArticleDuplicates";
import {
    downloadContentImages,
    getImagesUrls,
    getSourcesUrls
} from "@features/04-community/data/downloadArticlesContentImages";


const Page = () => {
    return <div className={'pt-32'}>
        <Button onClick={() => downloadArticleImages()}>Скачать картинки</Button>
        <Button onClick={() => putObject()}>залить картинки в селектел</Button>
        <Button onClick={() => cleanArticleDuplicates()}>Почистить дубликаты статей</Button>
        <Button onClick={() => getImagesUrls()}>Найти все картинки в статьях</Button>
        <Button onClick={() => getSourcesUrls()}>Найти все сорсы в статьях</Button>
        <Button onClick={() => downloadContentImages()}>Залить все картинки из статей на селектел</Button>
    </div>
}

export default Page;