import type { MetadataRoute } from 'next'
import {master} from "@managers/PrismaManager";

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
    const host = 'https://pet4u.tech';
    const articles = (await master.article.findMany({select: {id: true}})).map(item => ({url: `${host}/community/${item.id}`, lastModified: new Date(), changeFrequency: 'monthly',})) as {url: string, lastModified: Date, changeFrequency: 'monthly'}[];
    const ads = (await master.pet.findMany({select: {id: true}})).map(({id}) => ({url: `${host}/ad/${id}`, lastModified: new Date(), changeFrequency: 'monthly' as const}))
    return [
        {
            url: `${host}/`,
            lastModified: new Date(),
            changeFrequency: 'yearly',
        },
        {
            url: `${host}/landing/buyer`,
            lastModified: new Date(),
            changeFrequency: 'weekly',
            priority: 1,
        },
        {
            url: `${host}/landing/seller`,
            lastModified: new Date(),
            changeFrequency: 'weekly',
            priority: 0.8
        },
        {
            url: `${host}/contact-us`,
            lastModified: new Date(),
            changeFrequency: 'weekly',
        },
        {
            url: `${host}/signup`,
            lastModified: new Date(),
            changeFrequency: 'monthly',
        },
        {
            url: `${host}/signup/process`,
            lastModified: new Date(),
            changeFrequency: 'weekly',
        },
        {
            url: `${host}/community`,
            lastModified: new Date(),
            changeFrequency: 'weekly',
        },
        {
            url: `${host}/search`,
            lastModified: new Date(),
            changeFrequency: 'monthly',
        },
        ...articles,
        ...ads,
    ]
}