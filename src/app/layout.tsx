import './globals.css'
import {jost, montserratAlternates} from "../../utils/fonts";
import {Header} from "@features/01-init/components/Header";
import {Footer} from "@features/01-init/components/Footer";
import {Metadata} from "next";
import {Providers} from "@components/Providers";
import Head from "next/head";

export const metadata: Metadata = {
    title: 'Pet4U',
    description: 'Платформа для покупки и продажи породистых питомцев',
    keywords: ['Купить', 'Кошку', 'Собаку', 'Питомец', ''],
    other: {
       'yandex-verification': '182262a0f27c5fb2'
    }
}

export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <html lang="ru">

        <body className={`${jost.variable} ${montserratAlternates.variable}`}>
        <Header/>
        <Providers>
            {children}
        </Providers>
        <Footer/>
        </body>
        </html>
    )
}
