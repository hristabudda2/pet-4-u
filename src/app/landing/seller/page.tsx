import { Header } from '../../../../features/01-init/components/Header';
import { UTP } from '../../../../features/01-init/components/UTP';
import {About} from "../../../../features/01-init/components/About";
import {HowItWorks} from "../../../../features/01-init/components/HowItWorks";
import {ParticipateInTests} from "../../../../features/01-init/components/ParticipateInTests";
import {Footer} from "../../../../features/01-init/components/Footer";


export default function Page() {
    return <div>
        <UTP actor={'seller'}/>
        <About actor={'seller'}/>
        <HowItWorks actor={'seller'}/>
        <ParticipateInTests/>
    </div>
}
