import {UTP} from "@features/01-init/components/UTP";
import {About} from "@features/01-init/components/About";
import {HowItWorks} from "@features/01-init/components/HowItWorks";
import {ParticipateInTests} from "@features/01-init/components/ParticipateInTests";


export default function Page() {
    return <div>
        <UTP actor={'buyer'}/>
        <About actor={'buyer'}/>
        <HowItWorks actor={'buyer'}/>
        <ParticipateInTests/>
    </div>
}