import {getSearchPage} from '@features/06-search/data/getSearchPage';
import {pet, petGallery} from '@prisma-client';
import {ClientPage} from '@features/06-search/ClientPage';
import {FilterState, FilterStateOrUndefined} from '@features/06-search/Filters';

export type PetListView = Pick<pet, 'id' | 'birthday' | 'price' | 'name' | 'description' | 'createdAt'> & {breeder: {address: {description: string} | null, name: string} | null} & {gallery: petGallery[]}
const castSearchParamsToFilters: (params: Partial<FilterStateOrUndefined>) => FilterStateOrUndefined = (params: Partial<FilterState>) => ({breeder: params.breeder && params.breeder !== 'undefined'? Number(params.breeder): undefined, sex: params.sex && params.sex !== 'undefined'? params.sex: undefined, priceFrom: params.priceFrom && params.priceFrom !== 'undefined'? Number(params.priceFrom): undefined, priceTo: params.priceTo && params.priceTo !== 'undefined'? Number(params.priceTo): undefined})
const Page = async (props) => {
  const filters = castSearchParamsToFilters(await props.searchParams)



    const preloadedAds = await getSearchPage({page: 0, filters,});

    return <>
      <ClientPage filtersPreset={filters} preloadedAds={preloadedAds}/>
    </>

}

export default Page;