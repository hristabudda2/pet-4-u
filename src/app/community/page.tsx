'use client'



import {useEffect, useMemo, useState} from "react";
import {ArticleLightView, getArticles} from "@features/04-community/data/getArticles";
import {ArticleChip} from "@features/04-community/ArticleChip";
import 'react-virtualized/styles.css';
import InfiniteScroll from 'react-infinite-scroll-component';
import {ArticleSkeleton} from '@features/04-community/ArticleSkeleton';

const PAGE_SIZE = 20;

const Page = () => {
    const [articles, setArticles] = useState<ArticleLightView[]>([]);
    const [lastLoadLength, setLastLoadLength] = useState(20);
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);

    useEffect(() => {
        getArticles({skip: 0, take: PAGE_SIZE}).then(res => setArticles(res));
    }, [])

    const fetchNext = async () => {
        setLoading(true);
        const nextPage = await getArticles({skip: page * PAGE_SIZE, take: PAGE_SIZE});
        setLoading(false);
        setLastLoadLength(nextPage.length);
        setPage(prev => prev + 1)
        setArticles(prev => ([...prev, ...nextPage]))
    }

    const toList = (article: ArticleLightView) => <ArticleChip key={article.id} {...article}/>
    // const skeletonList = useMemo(() => new Array(20).fill(0).map(() =><ArticleSkeleton/>), [])

    return <InfiniteScroll next={fetchNext} hasMore={lastLoadLength === PAGE_SIZE} loader={<></>}
                    dataLength={articles.length}>
        <div className={'bg-brand-bg flex flex-row flex-wrap gap-5 px-20 justify-center pt-32 pb-20'}>
            {articles.map(toList)}
        </div>
    </InfiniteScroll>

}

export default Page;