import {getArticle} from "@features/04-community/data/getArticle";
import {PageProps} from "@specs/helpers";
import parse from 'html-react-parser';

const Page = async (props) => {

    const article = await getArticle(Number(props.params.articleId))

    return <div className={'pt-32 pb-10 prose mx-auto w-full'}>
        <h1>{article?.title}</h1>
        {parse(article!.content)}
    </div>
}

export default Page;