'use client'
import Image from 'next/image';
import {Snowfall} from 'react-snowfall';


export default function Page() {
  return <div className={'flex flex-col prose justify-center items-center pt-32 pb-10 mx-auto'}>
    <Snowfall
      style={{
        position: 'fixed',
        width: '100vw',
        height: '100vh',
      }}
    />
    <Snowfall
      style={{
        position: 'fixed',
        width: '100vw',
        height: '100vh',
      }}
    />
    <Snowfall
      style={{
        position: 'fixed',
        width: '100vw',
        height: '100vh',
      }}
    />
    <Snowfall
      style={{
        position: 'fixed',
        width: '100vw',
        height: '100vh',
      }}
    />
    <Snowfall
      style={{
        position: 'fixed',
        width: '100vw',
        height: '100vh',
      }}
    />
    <Snowfall
      style={{
        position: 'fixed',
        width: '100vw',
        height: '100vh',
      }}
    />
    <p>
      Дорогая моя алиночка!
    </p>
    <Image width={500} height={500} src={'/new-year.webp'} alt={''}/>
    <span className={'text-center'}>Следующий новый год проведем в сергиевом посаде(а вдруг)</span>
    <p>
      Это был непростой год для нас. Начался он трагично, заканчивается чуть лучше.
      Надеюсь, у нас сохранится такая динамика и в 2026 год мы войдем уже с хорошим настроем и с хорошими обстоятельствами.
    </p>
    <Image src={'/new-year-1.webp'} alt={''} width={500} height={500}/>
      <span className={'text-center'}>Мы в ожидании следующего года☝️</span>
    <p>
      Мне очень нравится быть с тобой: вместе тюленить, ездить в путешествия, дарить тебе подарки.
      Без тебя бы мне плохо жилось. Я очень рад тому, что ты у меня есть, лапочка!
    </p>
    <p>
      Дорого иметь возможность любить и быть любимым. Давай сохраним это!<br/>
      С новым тебя 2025 годом. Будь спокойна, у нас все точно разрулится, а если кто-то будет этому сопротивляться - <b>я всех уничтожу.</b>
    </p>
    <Image src={'/new-year-2.webp'} alt={''} width={500} height={500}/>
    <span className={'text-center'}>Каждый раз ты так сладко спыш 🥺<br/>️</span>
    <p>
      P.S. Я соскучился, моя дорогая женщина!
    </p>
  </div>
}