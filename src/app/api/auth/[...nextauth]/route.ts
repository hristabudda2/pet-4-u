import NextAuth, {AuthOptions} from "next-auth"
import {PrismaAdapter} from '@auth/prisma-adapter';
import EmailProvider from "next-auth/providers/email";
import {PrismaClient} from '@prisma-client';

const authOptions: AuthOptions = {
  adapter: PrismaAdapter(new PrismaClient()),
  secret: process.env.NEXT_AUTH_SECRET,
  providers: [
    EmailProvider({
      from: process.env.EMAIL_FROM,
      server: {
        host: process.env.EMAIL_HOST,
        port: +process.env.EMAIL_PORT,
        secure: false,
        tls: {
          rejectUnauthorized: false,
        },
      }
    })
  ],
}

const handler = NextAuth(authOptions)

export { handler as GET, handler as POST }