import {Contacts} from "@features/03-contact-us/Contacts";
import {WriteToUs} from "@features/03-contact-us/WriteToUs";


const Page = () => {
    return <div className={'flex flex-col gap-10 px-60 pt-20 pb-20'}>
        <h1 className={'font-montserrat text-xl text-brand-black font-bold'}>Контакты</h1>
        <div className={'grid grid-cols-[0.6fr_1fr] gap-x-10'}>
            <Contacts/>
            <WriteToUs/>
        </div>
    </div>
};

export default Page