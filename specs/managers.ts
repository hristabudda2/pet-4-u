import { booking, breeder, pet, user } from '@prisma/client/master';
import { AddPetImagePayload, CreateBreederPayload, CreatePetPayload, RemovePetImagePayload } from '@specs/payload';
import { ListPetView, ListBreedView, PagePetView } from '@specs/views';
import { Optional } from '@specs/helpers';


export type WithPaginationHandler<Payload, Model> = (limit: number, page: number, payload?: Payload) => Promise<Model>;
type OkHandler<Payload> = (payload: Payload) => Promise<boolean>



export interface UserManager {
    bookPet: OkHandler<{ userId: user['id'], petId: pet['id'] }>
    listPets: WithPaginationHandler<undefined, ListPetView[]>
    showBreederPhone: (breederId: breeder['id']) => Promise<Optional<string>>
    showPet: (petId: pet['id'], userId: user['id']) => Promise<Optional<PagePetView>>
}

export interface BreedManager {
    list: WithPaginationHandler<undefined, ListBreedView[]>,
}

export interface BreederManager {
    listOwnedPets: WithPaginationHandler<undefined, ListPetView[]>
    createBreeder: OkHandler<CreateBreederPayload>;
    createPet: OkHandler<CreatePetPayload>;
    addPetImage: OkHandler<AddPetImagePayload>;
    removePetImage: OkHandler<RemovePetImagePayload>;
}

