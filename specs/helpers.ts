import { NextApiRequest, NextApiResponse } from 'next';


export type PaginationPayload = {take: number, skip: number};

export type Optional<Model> = Model | null | undefined;

export type RequestWithBody<Body> = Omit<NextApiRequest, 'body'> & {body: Body}

export type ApiHandler<Body, Result> = (req: RequestWithBody<Body>, res: NextApiResponse<Result>) => Promise<any>

export type GalleryItem = {src: string, }

export type DictionaryKey = string; //Перевести на нормальный тип для того, чтобы типизация выебывалась.

export type IdParam = {id: string};

export type SlugParam = {slug: string};

export type PageProps<PageParams extends Record<any, any>, SearchParams extends [...any]> = {params: PageParams, searchParams: Spread<SearchParams>}


type OptionalPropertyNames<T> =
    { [K in keyof T]-?: ({} extends { [P in K]: T[K] } ? K : never) }[keyof T];

type SpreadProperties<L, R, K extends keyof L & keyof R> =
    { [P in K]: L[P] | Exclude<R[P], undefined> };

type Id<T> = T extends infer U ? { [K in keyof U]: U[K] } : never

type SpreadTwo<L, R> = Id<
    & Pick<L, Exclude<keyof L, keyof R>>
    & Pick<R, Exclude<keyof R, OptionalPropertyNames<R>>>
    & Pick<R, Exclude<OptionalPropertyNames<R>, keyof L>>
    & SpreadProperties<L, R, OptionalPropertyNames<R> & keyof L>
    >;

export type Spread<A extends readonly [...any]> = A extends [infer L, ...infer R] ?
    SpreadTwo<L, Spread<R>> : unknown