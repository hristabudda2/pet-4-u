import { pet, petGallery, breeder, priceRange, booking, breed, breedGallery } from '@prisma/client/master';


export type ListPetView = (pet & {gallery: petGallery[], breeder: breeder, priceRange: priceRange | null, booking: booking[]});
export type ListBreedView = breed & { gallery: breedGallery[] };
export type PagePetView = (pet & {gallery: petGallery[], breeder: breeder, priceRange: priceRange | null})
