import { pet, petGallery, Prisma } from '@prisma/client/master';
import breederCreateArgs = Prisma.breederCreateArgs;
import petCreateArgs = Prisma.petCreateArgs;


export type CreateBreederPayload = breederCreateArgs['data']
export type CreatePetPayload = petCreateArgs['data'];
export type AddPetImagePayload = {petId: pet['id'], image: File, }
export type RemovePetImagePayload = { galleryItemId: petGallery['id'] };