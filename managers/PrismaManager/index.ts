import MasterInstance from './master';

/**
 *  Тк имеем проблему с соединениями призмы к БД.
 *      И тк по факту сейчас в продакн используем один сервер БД,
 *      Временно отключаем второй клиент призмы. (но из кода не убираем, просто подменяем slave мастером)
 *      https://github.com/prisma/prisma/discussions/16490
 */

export const slave = MasterInstance;
export const master = MasterInstance;

