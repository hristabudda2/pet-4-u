import { PrismaClient as Master, PrismaClient } from '../../prisma/out';

const globalPrisma = global as unknown as { master: Master };
const master = globalPrisma.master || new Master({ log: [{ level: 'query', emit: 'event' }] });

if (process.env.NODE_ENV !== 'production') globalPrisma.master = master;

export default master;
