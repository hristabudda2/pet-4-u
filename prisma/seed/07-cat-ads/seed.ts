'use server';
import data from './data.json';
import {PrismaClient, Sex} from '@prisma-client';
import {PutObjectCommand, S3Client} from '@aws-sdk/client-s3';
import yauzl from 'yauzl';
import {nanoid} from 'nanoid';

export async function loadZipTos3AndCreateGallery(id: number, galleryLink: string, client: PrismaClient) {
  const s3 = new S3Client({
    credentials: {accessKeyId: process.env.ACCESS_KEY, secretAccessKey: process.env.SECRET_KEY},
    endpoint: process.env.S3_STORAGE_LINK,
    forcePathStyle: true,
    region: 'ru-1',
    apiVersion: 'latest',
  });
  const res = await fetch(galleryLink);
  const contentType = res.headers.get('Content-Type') || undefined;
  const fileBuffer = Buffer.from(await res.arrayBuffer())
  if (!res.ok) {
    throw 'file not downloaded'
  }
          const fileId = nanoid(8);
          const Key = `/galleries/${id}/${fileId}`
          console.log(galleryLink, contentType);
          await client.petGallery.create({data: {imageId: Key, petId: id}})
          await s3.send(
            new PutObjectCommand({
              BucketKeyEnabled: false,
              Bucket: process.env.S3_BUCKET_NAME,
              ContentType: contentType,
              Key,
              Body: fileBuffer,
            })
          );
}

export async function main() {
  const client = new PrismaClient({});
  const breeds = Array.from(new Set(data.map(item => item.breed)));
  await client.breed.createMany({data: breeds.map(name => ({name})), skipDuplicates: true});
  for (const ad of data) {
    const [d, m, y] = ad.birthDate.split('.');
    const pet = await client.pet.create({
      data: {
        name: ad.title,
        sex: ad.sex as Sex,
        birthday: new Date(`${m}.${d}.${y}`),
        price: ad.price,
        isOnSale: true,
        description: ad.description,
        breeder: {
          connectOrCreate: {
            where: {name: ad.sellerName},
            create: {name: ad.sellerName, description: ad.sellerDescription, phone: ad.sellerPhone || '',}
          }
        }
      }
    });
    for (const item of ad.gallery) {
      await loadZipTos3AndCreateGallery(pet.id, item, client)
    }
  }
}
main()

