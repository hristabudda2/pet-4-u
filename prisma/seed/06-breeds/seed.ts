import {PrismaClient} from '@prisma-client';
import * as data from './dog-breeds.json';

async function main() {
  const client = new PrismaClient();
  const createPayload = data.map(item => ({name: item}));
  await client.breed.createMany({data: createPayload})
}

main();