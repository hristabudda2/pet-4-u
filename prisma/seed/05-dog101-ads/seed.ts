import {PrismaClient} from '@prisma-client';
import * as data from './seed.json';
import yauzl from 'yauzl';
import {PutObjectCommand, S3Client} from '@aws-sdk/client-s3';
import fs from 'fs';
import assert from 'node:assert';

export async function loadZipTos3AndCreateGallery(id: number, galleryLink: string, client: PrismaClient) {
  const s3 = new S3Client({
    credentials: {accessKeyId: process.env.ACCESS_KEY, secretAccessKey: process.env.SECRET_KEY},
    endpoint: process.env.S3_STORAGE_LINK,
    forcePathStyle: true,
    region: 'ru-1',
    apiVersion: 'latest',
  });
  const res = await fetch(galleryLink.replace('dog101.ru', '101dog.ru'));
  if (!res.ok) {
    throw 'file not downloaded'
  }

  await new Promise(async (resolve, reject) => {
    yauzl.fromBuffer(Buffer.from(await res.arrayBuffer()), async (err, zipfile) => {
      if (err) {
        reject(err)
      }
      const processEntries = async (entry: yauzl.Entry) => {
        zipfile.openReadStream(entry, async (err, stream) => {
          const Key = `/galleries/${id}/${entry.fileName}`
          console.log(Key)
          await client.petGallery.create({data: {imageId: Key, petId: id}})
          await s3.send(
            new PutObjectCommand({
              BucketKeyEnabled: false,
              Bucket: process.env.S3_BUCKET_NAME,
              ContentLength: entry.uncompressedSize,
              ContentType: 'image/jpeg',
              Key,
              Body: stream,
            })
          );
        })
      }
      zipfile.on('entry', processEntries)
      zipfile.on('end', () => {
        zipfile.off('entry', processEntries)
        resolve(null)
      })
    })
  })
}

async function main() {
  const client = new PrismaClient();

  for (let i = 0; i < data.length; i++) {
    console.log(`processing ${i + 1} entry of ${data.length}`)
    const item = data[i];
    await client.$transaction(async (trx) => {
      const breeder = await trx.breeder.upsert({
        where: {
          name: item.breeder
        },
        update: {},
        create:
          {
            name: item.breeder,
            phone: item.phone,
            address: {
              create: {
                description: item.metroStation || 'Адрес не указан.',
                type: 'LINK',
              }
            }

          }
      })
      const [day, month, year] = item.birthDate.replace(' ', '').replace(/[а-я]+/ig, '').split('.');
      const birthday = [month, day, year].join('.')
      if (isNaN(Number(day)) || isNaN(Number(month)) || isNaN(Number(year))) {
        console.log(item.birthDate, birthday)
        throw 'Incorrect date'
      }

      await trx.pet.create({
        data: {
          id: i + 1,
          birthday: new Date(birthday),
          sex: item.title.toLowerCase().includes('девочка') ? 'Female' : 'Male',
          name: item.title,
          description: item.description,
          isOnSale: true,
          price: item.price,
          breederId: breeder.id
        }
      });
    });
    await loadZipTos3AndCreateGallery(i + 1, item.galleryLink, client)
  }
}


main()