'use server';
import { PrismaClient } from '@prisma-client';
import * as data from './seed.json';






async function main() {
    const client = new PrismaClient();
    return client.article.createMany({data: data.map(({tags, title, content, link, image_link}) => ({
            tale: null,
            cover: image_link,
            title,
            tags,
            content
        }))})
}

main().then((res) => {
    console.log(`Inserted ${res.count} rows`)
})