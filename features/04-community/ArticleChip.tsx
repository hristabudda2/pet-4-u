import {FC} from "react";
import {ArticleLightView} from "@features/04-community/data/getArticles";
import Image from "next/image";
import dayjs from "dayjs";
import Link from "next/link";


export const ArticleChip: FC<ArticleLightView> = ({id, tags, createdAt, title, cover, tale}) => {
    return <Link href={`/community/${id}`} key={id} className={'flex flex-col max-w-[334px] gap-5 rounded-t-[50px] bg-white'}>
        <Image width={334} height={334} src={cover} alt={title} className={'rounded-t-[50px]'}/>
        <div className={'flex flex-col gap-2.5 p-5'}>
            <h1 className={'text-brand-blue-100 font-montserrat font-bold text-lg'}>{title}</h1>
            <h3>{tale}</h3>
            <span className={'font-jost text-sm text-brand-gray-100'}>{dayjs(createdAt).format('DD.MM.YYYY')}</span>
        </div>
    </Link>
}