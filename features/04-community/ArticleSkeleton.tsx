import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'


export const ArticleSkeleton = () => {
  return <div className={'flex flex-col max-w-[334px] gap-5 rounded-t-[50px] bg-white'}>
    <Skeleton height={334} width={334} className={'!rounded-t-[50px]'} inline={true} style={{lineHeight: 'unset'}}/>
    <div className={'flex flex-col gap-2.5 p-5'}>
      <Skeleton/>
      <Skeleton/>
      <Skeleton/>
    </div>
  </div>
}