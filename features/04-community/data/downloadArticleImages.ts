'use server';



import {master} from "@managers/PrismaManager";
import * as fs from "node:fs";
import {nanoid} from "nanoid";

export async function getStatusPromise(actionFn) {
    try {
        const data = await actionFn();

        return {
            isOk: true,
            data,
        };
    } catch (error) {
        console.error(error);

        return {
            isOk: false,
        };
    }
}

export const sleep = async (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const fetchAndWriteCover = async (url: string) => {
    const response = await fetch(url);
    if (response.status === 503) {
        throw 'error while downloading'
    }

    const path = new URL(url).pathname;
    const ext = response.headers.get('Content-Type')?.replace('image/', '');
    if (response.headers.get('Content-Type')?.includes('image/')) {
        const fileId = `${nanoid(8)}.${ext}`
        console.log(`file id: ${fileId}`)
        await master.fileToUrl.create({
            data: { path, fileId }
        })

        fs.writeFileSync(`./sites/${fileId}`, Buffer.from(await response.arrayBuffer()), {})
    }

}

export async function downloadArticleImages() {
    const articles = await master.article.findMany({select: {cover: true}});
    let counter = 1;
    for (const {cover} of articles) {

        console.log(`downloading article #${counter}/${articles.length}`)
        while (!(await getStatusPromise(() => fetchAndWriteCover(cover))).isOk) {
            await sleep(1500)
        }
        await sleep(300);
        counter++
    }
}