'use server';



import {article} from "@prisma-client";
import {master} from "@managers/PrismaManager";

export async function getArticle(id: article['id']) {
    return master.article.findUnique({where: {id}})
}