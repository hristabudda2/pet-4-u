'use server';

import {master} from "@managers/PrismaManager";
import {PutObjectCommand, S3Client} from "@aws-sdk/client-s3";
import fs from 'fs';

export async function putObject() {
    const s3 = new S3Client({
        credentials: { accessKeyId: process.env.ACCESS_KEY, secretAccessKey: process.env.SECRET_KEY },
        endpoint: process.env.S3_STORAGE_LINK,
        forcePathStyle: true,
        region: 'ru-1',
        apiVersion: 'latest'
    });
    const files = await master.fileToUrl.findMany();
    for (const {path: Key, fileId} of files) {
        try {
            console.log(`upload ${decodeURI(Key)}`)
            const data = await s3.send(
                new PutObjectCommand({
                    BucketKeyEnabled: false,
                    Bucket: process.env.S3_BUCKET_NAME,
                    ContentType: Key.includes('.png') ? 'image/png': 'image/jpg',
                    Key: decodeURI(Key),
                    Body: fs.readFileSync(`/Users/adm/IdeaProjects/pet4u/sites/${fileId}`),
                })
            );

        } catch (e) {
            console.error(e)
            console.error(`Error on file with id #${fileId}`)
        }
    }
    return 'ok'
}
