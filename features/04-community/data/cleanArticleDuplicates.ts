'use server';


import {master} from "@managers/PrismaManager";

export async function cleanArticleDuplicates() {
    const articles = await master.article.findMany();
    const articlesGroupedByTitle = articles.reduce((obj, item) => {
        const prevArr = obj[item.title] ?? [];
        return {...obj, [item.title]: [...prevArr, item.id]}
    }, {});
    let counter = 0;
    const articlesToDelete: number[] = [];
    for (const key in articlesGroupedByTitle) {
        const articlesGroup = articlesGroupedByTitle[key]
        if (articlesGroup.length > 1) {
            for (let i = 1; i < articlesGroup.length; i++) {
                articlesToDelete.push(articlesGroup[i])
            }
        }

    }
    await master.article.deleteMany({where: {id: {in: articlesToDelete}}})
}