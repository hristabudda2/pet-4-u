'use server'

import {master} from "@managers/PrismaManager";
import {article} from "@prisma-client";
export type ArticleLightView = Pick<article, 'id' | 'cover' | 'title' | 'tags' | 'createdAt' | 'tale'>

export async function getArticles(pagination: {skip: number, take: number}): Promise<ArticleLightView[]> {
    return master.article.findMany({...pagination, orderBy: {id: 'desc'}, select: {id: true, cover: true, title: true, tags: true, createdAt: true, tale: true}})
}