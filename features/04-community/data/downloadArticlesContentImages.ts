'use server';


import {master} from "@managers/PrismaManager";
import fs from "fs";
// import data from '../../../imagesToLoad.json'
import {nanoid} from "nanoid";
import {PutObjectCommand, S3Client} from "@aws-sdk/client-s3";
import {getStatusPromise, sleep} from "@features/04-community/data/downloadArticleImages";

export async function getImagesUrls() {
    const articles = await master.article.findMany();
    const imagesToDownload: string[] = [];
    for (const article of articles) {
        const imgTags = article.content.match(/<img [^>]*src="[^"]*"[^>]*>/gm);
        if (imgTags !== null) {
            for (const tag of imgTags) {
                imagesToDownload.push(tag.replace(/.*src="([^"]*)".*/, '$1'))
            }
        }
    }
    fs.writeFileSync('./imagesToLoad.json', JSON.stringify(imagesToDownload, null, 2))
}

export async function getSourcesUrls() {
    const articles = await master.article.findMany();
    const imagesToDownload: string[] = [];
    for (const article of articles) {
        const imgTags = article.content.match(/<source [^>]*srcset="[^"]*"[^>]*>/gm);
        if (imgTags !== null) {
            for (const tag of imgTags) {
                imagesToDownload.push(tag.replace(/.*srcset="([^"]*)".*/, '$1'))
            }
        }
    }
    fs.writeFileSync('./imagesToLoad.json', JSON.stringify(imagesToDownload, null, 2))
}

export async function downloadContentImages() {
    let counter = 0;
    for (const linkRaw of ['']) {
        const link = linkRaw.replace(' 1x', '');
        console.log(`Скачиваю картинку #${counter}, ссылка: ${link}`)

        if (link.includes('https://')) {
            while (!(await getStatusPromise(() => fetchAndWriteCover(link))).isOk) {
                await sleep(5000)
            }
        }

        if (link.startsWith('/sites')) {
            while (!(await getStatusPromise(() => fetchAndWriteCover(`https://proplan.ru${link}`))).isOk) {
                await sleep(5000)
            }
        }
        counter++
    }
}

const fetchAndWriteCover = async (url: string) => {
    const response = await fetch(url);
    if (response.status === 503) {
        throw 'error while downloading'
    }
    if (response.status === 404) {
        return;
    }
    if (response.status === 403) {
        return;
    }
    if (!response.ok) {
        console.log(response)
        throw 'response not ok';
    }
    const link = new URL(url)
    const s3 = new S3Client({
        credentials: {accessKeyId: process.env.ACCESS_KEY, secretAccessKey: process.env.SECRET_KEY},
        endpoint: process.env.S3_STORAGE_LINK,
        forcePathStyle: true,
        region: 'ru-1',
        apiVersion: 'latest'
    });
    if (response.headers.get('Content-Type')?.includes('image/')) {

        const buffer = Buffer.from(await response.arrayBuffer());
        await s3.send(
            new PutObjectCommand({
                BucketKeyEnabled: false,
                Bucket: process.env.S3_BUCKET_NAME,
                ContentType: response.headers.get('Content-Type')!,
                Key: decodeURI(link.pathname),
                Body: buffer,
            })
        );
    }

}