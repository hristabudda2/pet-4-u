import {FC} from 'react';
import Image from 'next/image';


export const AdGallery: FC<{links: string[]}> = ({links}) => {
  if (links.length > 1) {
    const [firstLink, ...otherLinks] = links;
    return <div className={'grid grid-cols-2'}>
      <div>
        <Image fill={true} src={firstLink} alt={'Заглавная картинка галереи'}/>
      </div>
      <div className={'grid grid-cols-3'}>
        {otherLinks.map((item, i) => <Image key={i} src={item} alt={item}/>)}
      </div>
    </div>
  }
  return null;
}