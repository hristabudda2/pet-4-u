'use server';


import {pet} from '@prisma/client/master';
import {master} from '@managers/PrismaManager';
import {getServerSession} from 'next-auth';

export async function getAdPhone(id: pet['id']) {
  const session = getServerSession();

  if (!session) {
    return {message: 'Forbidden', code: 403}
  } else {
    return master.pet.findUnique({where: {id}, select: {breeder: {select: {phone: true}}}}).then(res => res?.breeder.phone || '')
  }
}