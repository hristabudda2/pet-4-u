'use server';


import {breeder} from '@prisma-client';
import {master} from '@managers/PrismaManager';

export async function getBreederById(id: breeder['id']) {
  return master.breeder.findFirst({where: {id}, select: {name: true, id: true}})
}