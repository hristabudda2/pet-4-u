'use server';

import {master} from '@managers/PrismaManager';
import {FilterState} from '@features/06-search/Filters';


export async function getSearchPage({page, filters}: { page: number, filters: Partial<FilterState> }) {
  return master.pet.findMany({
    where: {
      price: {
        gte: filters.priceFrom,
        lte: filters.priceTo,
      },
      breeder: {
        id: filters.breeder
      },
      sex: filters.sex
    },
    take: Number(process.env.NEXT_PUBLIC_SEARCH_PAGE_SIZE),
    skip: page * Number(process.env.NEXT_PUBLIC_SEARCH_PAGE_SIZE),
    select: {
      id: true,
      description: true,
      birthday: true,
      name: true,
      price: true,
      createdAt: true,
      gallery: true,
      breeder: {
        select: {
          name: true,
          address: {
            select: {
              description: true
            }
          }
        }
      }
    }
  })
}