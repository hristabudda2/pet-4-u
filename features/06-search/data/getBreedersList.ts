'use server';


import {master} from '@managers/PrismaManager';

export async function getBreedersList() {
  return master.breeder.findMany({select: {id: true, name: true}}).then(res => res.map(item => ({value: item.id, label: item.name})));
}