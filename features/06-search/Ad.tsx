import {FC} from 'react';
import {Icon} from '@components/Icon/Icon';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime'
import updateLocale from 'dayjs/plugin/updateLocale'
import {PetListView} from '@/app/search/page';
import {Gallery} from '@features/06-search/Gallery';
import {Button} from '@components/Button';
import {OpenPopupFn} from '@hooks/usePopup';
import {LoginPopup} from '@features/06-search/LoginPopup';

dayjs.extend(updateLocale)
dayjs.extend(relativeTime)
dayjs.locale('ru')

const SYMBOLS_LIMIT = 175;


export const Ad: FC<PetListView & {open: OpenPopupFn}> = ({id, name, price, birthday, breeder, description, gallery, createdAt, open}) => {
  const limitedDescription = description?.slice(0, SYMBOLS_LIMIT).trim();
  return <div className={'flex flex-row gap-5 rounded-lg bg-white overflow-hidden'}>
    <Gallery links={gallery.map(item => item.imageId)}/>
    <div className={'flex flex-col pt-8 pr-8 pb-8'}>
      <div className={'flex flex-row justify-between mb-7'}>
        <h1 className={'font-montserrat font-bold text-lg text-brand-black'}>{name}</h1>
        <h1 className={'text-2xl text-brand-orange-100 font-montserrat font-bold'}>{price} ₽</h1>
      </div>
      <div className={'flex flex-row gap-5 mb-5'}>
        <div className={'flex flex-row gap-2 items-center'}>
          <Icon.birthday/>
          <h2 className={'text-brand-gray-100 font-jost text-sm'}>{dayjs(birthday).fromNow(true)}</h2>
        </div>
        <div className={'flex flex-row gap-1 items-center'}>
          <Icon.geo/>
          <h2 className={'text-brand-gray-100 font-jost text-sm'}>{breeder?.address?.description}</h2>
        </div>
      </div>
      <span className={'prose w-[644px] mb-5'}>{limitedDescription? `${limitedDescription}${limitedDescription.endsWith('.')? '..': '...'}`: description}</span>
      <div className={'flex flex-row justify-between w-full bg-brand-bg py-3 px-5 rounded-lg mb-5'}>
        <div className={'flex flex-row '}>
          <h3 className={'font-jost text-sm text-brand-gray-100'}>{breeder?.name}</h3>
        </div>
        <div></div>
      </div>
      <div className={'flex flex-row justify-between items-center'}>
        <div className={'font-jost text-brand-gray-100 text-sm'}>{dayjs(createdAt).format('DD.MM.YYYY')}</div>
        <div><Button onClick={() => open(<LoginPopup adId={id}/>)} variant={'outlined'} className={'!border-brand-blue-100 !px-20'} textClassName={'!text-brand-blue-100'}>Контакты</Button></div>
      </div>
    </div>
  </div>
}

