import {FC, useMemo, useState} from 'react';
import Image from 'next/image';


export const Gallery: FC<{links: string[]}> = ({links}) => {
  const [src, setSrc] = useState(links[0]);
  const [selectedIndex, setSelectedIndex] = useState(0)

  const imageSwitchers = useMemo(() => {
    const count = links.length
    const percent = Math.ceil(100 / count);

    return new Array(links.length)
      .fill(0)
      .map((_, i) => <div key={`image-switcher-${i}`}
      style={{position: 'absolute', left: `${percent * i}%`, top: 0, height: 'calc(100% - 15px)', width: `${percent}%`, zIndex: 10}} onMouseEnter={() => {
        setSrc(links[i]);
        setSelectedIndex(i)
      }}/>)

  }, [selectedIndex])
  const indicators = useMemo(() => {
    const count = links.length
    const percent = Math.ceil(50 / count);

    return new Array(links.length).fill(0).map((_, i) => <div key={`indicator-${i}`} style={{width: `${percent - 3}%`, zIndex: 12, height: '2px', position: 'absolute', bottom: 10, left: `${25 + percent * i}%`, background: i === selectedIndex? '#F5732A': '#D5E1EB', cursor: 'pointer'}} onClick={() => {
      setSelectedIndex(i);
      setSrc(links[i])
    }}/>)
  }, [selectedIndex])
  return <div className={'relative w-[334px] h-[386px]'}>
    <Image className={'object-contain z-[5]'} fill={true} sizes={'(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'} src={`https://eb12ccf9-ce7e-48b4-97b0-fc77ac31af1d.selstorage.ru/pet4u${src}`} alt={''}/>
    <Image className={'opacity-70'} fill={true} sizes={'(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw'} src={`https://eb12ccf9-ce7e-48b4-97b0-fc77ac31af1d.selstorage.ru/pet4u${src}`} alt={''}/>
    {links.length > 1 && indicators}
    {links.length > 1 && imageSwitchers}
  </div>
}