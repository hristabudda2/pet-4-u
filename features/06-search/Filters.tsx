import {Input} from '@components/Common/Input';
import Select from 'react-select';
import {ChangeEvent, ChangeEventHandler, FC, useEffect, useState} from 'react';
import {Sex} from '@prisma-client';
import {getBreederById} from '@features/06-search/data/getBreederById';
import {getBreedersList} from '@features/06-search/data/getBreedersList';
import {FilterInput} from '@components/Common/FilterInput';

type FilterFields = 'priceFrom' | 'priceTo' | 'sex' | 'breeder';
type ChangeFilterField = (key: FilterFields, value: any) => void;

export type FilterState = Record<FilterFields, any>;
export type FilterStateOrUndefined = Record<FilterFields, any | undefined>;

export const Filters: FC<{state: FilterState, onChange: ChangeFilterField}> = ({state, onChange}) => {
  const [breederList, setBreederList] = useState<{value: number, label: string}[]>([]);
  const sexes = [{value: Sex.Male, label: 'Мальчик'}, {value: Sex.Female, label: 'Девочка'}]
  const sexByValue = (value: Sex) => sexes.find(item => item.value === value)
  const breederByValue = (value: number) => breederList.find(item => item.value === value)
  useEffect(() => {
    if (state.breeder)
    getBreederById(state.breeder).then(res => setBreederList(prev => ([...prev, {label: res!.name, value: res!.id}])))
  }, []);
  const fetchBreedersOnOpen = () => {
    if (breederList.length <= 1)
    getBreedersList().then(res => setBreederList(prev => ([...prev, ...res])))
  }
  const inputChangeHandler = (key: FilterFields) => (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
    onChange(key, Number(e.target.value || '0'))
  }
  const selectChangeHandler = (key: FilterFields) => (newValue) => onChange(key, newValue?.value)
  return <div className={'mr-5 rounded-lg flex flex-col bg-white py-8 px-5'}>
    <h1 className={'font-montserrat text-lg font-bold mb-[30px]'}>Фильтры</h1>
    <section className={'flex flex-row mb-10 gap-3.5 items-center'}>
      <FilterInput value={state.priceFrom} placeholder={'Цена от'} onChange={inputChangeHandler('priceFrom')}/>
      <div>-</div>
      <FilterInput value={state.priceTo} placeholder={'Цена до'} onChange={inputChangeHandler('priceTo')}/>
    </section>
    <Select isClearable={true} className={'mb-8'} placeholder={'Пол'} options={sexes} onChange={selectChangeHandler('sex')} value={sexByValue(state.sex)}/>
    <Select isClearable={true} placeholder={'Заводчик'} options={breederList} onMenuOpen={fetchBreedersOnOpen} onChange={selectChangeHandler('breeder')} value={breederByValue(state.breeder)}/>
  </div>
}