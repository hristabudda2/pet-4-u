'use client'


import {FC, useEffect, useState} from 'react';
import {PetListView} from '@/app/search/page';
import {getSearchPage} from '@features/06-search/data/getSearchPage';
import {Ad} from '@features/06-search/Ad';
import InfiniteScroll from 'react-infinite-scroll-component';
import {Filters, FilterState, FilterStateOrUndefined} from '@features/06-search/Filters';
import {useRouter} from 'next/navigation';
import {usePopup} from '@hooks/usePopup';
import {LoginPopup, LoginPopupProps} from '@features/06-search/LoginPopup';

export const ClientPage: FC<{preloadedAds: PetListView[], filtersPreset: FilterStateOrUndefined}> = ({preloadedAds, filtersPreset}) => {
  const [page, setPage] = useState(0)
  const router = useRouter();
  const {Popup, open, isOpen} = usePopup<LoginPopupProps>()
  const [ads, setAds] = useState<PetListView[]>(preloadedAds)
  const [filters, setFilters] = useState<FilterState>(filtersPreset);
  const [lastLoadLength, setLastLoadLength] = useState(20);
  useEffect(() => {
    setPage(0)
    getSearchPage({page: 0, filters}).then(setAds);
    // if (JSON.stringify(filters) !== JSON.stringify({breeder: undefined, sex: undefined, priceFrom: undefined, priceTo: undefined}))
    const search = new URLSearchParams(filters).toString();
    router.push(`search?${search}`)

  }, [JSON.stringify(filters)])
  const fetchNext = async () => {
    const nextPage = await getSearchPage({page: page + 1, filters});
    setLastLoadLength(nextPage.length);
    setPage(prev => prev + 1)
    setAds(prev => ([...prev, ...nextPage]));
  }
  const toAdList = (item: PetListView) => <Ad key={item.id} {...item} open={open}/>
  return <div className={`grid grid-cols-[1fr_2fr] bg-brand-bg px-10 pt-32 pb-10 ${isOpen && 'overflow-y-hidden'}`}>
    <Popup/>
    <Filters state={filters} onChange={(key, value) => setFilters(prev => ({...prev, [key]: value}))}/>
    <InfiniteScroll next={fetchNext} hasMore={lastLoadLength === Number(process.env.NEXT_PUBLIC_SEARCH_PAGE_SIZE)} loader={<></>}
                              dataLength={ads.length}><div className={'flex flex-col justify-center items-center gap-5'}>{ads.map(toAdList)}</div></InfiniteScroll></div>
}