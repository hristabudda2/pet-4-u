import {FC, useEffect, useState} from 'react';
import {getAdPhone} from '@features/06-search/data/getAdPhone';
import {pet} from '@prisma/client/master';
import {Button} from '@components/Button';
import Link from 'next/link';

export type LoginPopupProps = {adId: pet['id']};

export const LoginPopup: FC<LoginPopupProps> = ({adId}) => {
  const [phone, setPhone] = useState<string | null>(null);
  const [isAuthorized, setIsAuthorized] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getAdPhone(adId).then((phone) => {
      if (typeof phone === 'object') {
        if ('code' in phone && phone.code === 403)
        setIsAuthorized(false)
      } else {
        setPhone(phone);
        setIsAuthorized(true);
      }
      setIsLoading(false);
    })
  }, []);


  if (isLoading)
    return null;

  if (!isAuthorized)
  return <div className={'flex flex-col prose items-center justify-center'}>
    <h1 className={'font-montserrat'}>Не так быстро!</h1>
    <span className={'font-jost font-bold'}>Вы не авторизованы!</span>
    <span className={'mb-10'}>Чтобы получить доступ к контактам - авторизуйтесь.</span>
    <div className={'flex flex-row justify-between gap-3'}>

    <Link href={'/signup'}><Button actor={'seller'}>Регистрация</Button></Link>
    <Button>Вход</Button>
    </div>
  </div>

  if (isAuthorized)
  return <div className={'flex flex-col prose items-center px-16 py-5'}>
    <h1>Контакты:</h1>
    <span><span className={'font-bold font-montserrat'}>Телефон:</span> <Link href={`tel:${phone}`}>{phone}</Link></span>
    <span>Скажите продавцу что нашли его объявление на сайте pet4u!</span>
  </div>
}