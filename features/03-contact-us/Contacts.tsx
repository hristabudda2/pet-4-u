import {Icon} from "@components/Icon/Icon"
import Link from "next/link";
import {FC, PropsWithChildren} from "react";


export const Contacts = () => {
    return <div className={'flex flex-col gap-10'}>
        <GroupCell>
            <Icon.phone/>
            <Link href={'tel:+79059301437'}><GroupTitle>+7 905 930 14 37</GroupTitle></Link>
        </GroupCell>
        <GroupCell>
            <Icon.tgOutlined/>
            <Link target={'_blank'} href={'https://t.me/q6int'}><GroupTitle>Написать в чат Telegram</GroupTitle></Link>
        </GroupCell>
    </div>
}

const GroupCell: FC<PropsWithChildren> = ({children}) => {
    return <div className={'flex flex-row gap-6'}>{children}</div>
}

const GroupTitle: FC<PropsWithChildren> = ({children}) => <span
    className={'text-brand-blue-100 font-montserrat font-bold text-xl'}>{children}</span>
