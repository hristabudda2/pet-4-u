'use server';


import {WriteToUsForm} from "@features/03-contact-us/WriteToUs";
import {master} from "@managers/PrismaManager";

export async function sendMessage(data: WriteToUsForm) {
    return master.message.create({data})
}