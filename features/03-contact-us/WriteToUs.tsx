'use client';
import {WithLabel} from "@features/02-login/components/WithLabel";
import {FormInput} from "@features/02-login/components/FormInput";
import {useForm} from "react-hook-form";
import {Button} from "@components/Button";

export type WriteToUsForm = {
    name: string;
    email: string;
    message: string;
}

export const WriteToUs = () => {
    const {register, handleSubmit} = useForm<WriteToUsForm>();
    return <div className={'w-full rounded-[30px] p-7 bg-brand-blue-50 flex flex-col gap-10'}>
        <h3 className={'font-montserrat font-bold text-2xl text-brand-black'}>Напишите нам</h3>
        <form className={'flex flex-col gap-5'} onSubmit={handleSubmit(async (data) => {

        })}>
            <WithLabel label={'Ваше имя'}>
                <FormInput {...register('name')}/>
            </WithLabel>
            <WithLabel label={'Ваш e-mail'}>
                <FormInput {...register("email")}/>
            </WithLabel>
            <WithLabel className={'mb-10'} label={'Ваше сообщение'}>
                <textarea {...register('message')} className={'font-montserrat text-sm px-3 py-5 w-full min-h-36 rounded-[10px] border border-brand-gray-50'} placeholder={'Сообщение'}/>
            </WithLabel>
            <Button actor={'seller'} type={'submit'}>Отправить сообщение</Button>
        </form>
    </div>
}