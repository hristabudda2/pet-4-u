import {Chip} from "./Chip/Chip";
import {useMemo} from "react";


export const About = ({actor}) => {
    const actorDictKeys = {
        seller: {
            why_us: 'Pet4U — это платформа для продажи животных. Сервис бережет вас от хлопот и недобросовестных покупателей. Уделите больше внимания себе, своим близким и своим животным - для сделок есть pet4u',
            first_chip_title: 'Целевая аудитория',
            first_chip_desc: 'Вас увидят покупатели, не задающие вопрос "почему так дорого"',
            second_chip_title: 'Маркетинг',
            second_chip_desc: 'Ваши объявления увидят, мы об этом позаботимся',
            third_chip_title: 'Спокойствие',
            third_chip_desc: 'Выращивайте животных, остальное - наша забота',
        },
        buyer: {
            why_us: 'Pet4U — это платформа для покупки животных. Сервис бережет вас от хлопот и недобросовестных продавцов. Уделите внимание выбору питомца - для сделок есть pet4u',
            first_chip_title: 'Здоровые животные',
            first_chip_desc: 'Продавцов проверяем с помощью кинологов и ветеринаров.',
            second_chip_title: 'Членство в РКФ',
            second_chip_desc: 'Питомники на нашей платформе состоят в РКФ. Мы за этим следим.',
            third_chip_title: 'Стандарт контента',
            third_chip_desc: 'Мы отправляем фотографа делать снимки животных, вы увидите все, что хотите',
        }
    }
    const actorKeys = useMemo<typeof actorDictKeys['buyer']>(() => actorDictKeys[actor], [actor]);
    return <section className={'grid grid-cols-1 gap-y-5 md:grid-cols-[1fr_0.9fr] pt-20 px-4 md:px-60 gap-x-4 mb-16'}>
        <span className={'flex flex-col gap-[10px]'}>
            <div className={'text-left text-sm text-brand-orange-100 font-jost'}>
                О НАС
            </div>
            <div className={'font-montserrat font-bold text-6xl'}>Почему стоит выбрать Pet4U</div>
            <div className={'text-brand-gray-100 text-lg font-jost'}>{actorKeys.why_us}</div>
        </span>
        <div className={'flex flex-col gap-[20px]'}>
            <Chip actor={actor} title={actorKeys.first_chip_title}
                  description={actorKeys.first_chip_desc}/>
            <Chip actor={actor} title={actorKeys.second_chip_title} description={actorKeys.second_chip_desc}/>
            <Chip actor={actor} title={actorKeys.third_chip_title} description={actorKeys.third_chip_desc}/>
        </div>
    </section>
}