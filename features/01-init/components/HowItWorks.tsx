import {VerticalChip} from "./VerticalChip";
import {Icon} from "../../../components/Icon/Icon";
import {useMemo} from "react";


export const HowItWorks = ({actor}) => {
    const actorDict = {
        seller: {
            first_chip_title: 'Простой старт',
            first_chip_desc: 'Три клика, справка из РКФ, и вы уже можете выставлять своих питомцев на продажу. Это просто. Проверьте.',
            second_chip_title: 'Пользователи',
            second_chip_desc: 'Наши пользователи поддерживают правильные питомники. Если вы из их числа - вам понравится',
            third_chip_title: 'Безопасное взаимодействие',
            third_chip_desc: 'Наша нейросеть ответит на глупые и необдуманные вопросы покупателей. А то, что посчитает требующим вашего внимания - передаст вам.',
            fourth_chip_title: 'Совершенные сделки',
            fourth_chip_desc: 'Мы выступаем гарантом сделок и вашего спокойствия. Покупатель сначала передает деньги нам, и только после получает адрес вашего питомника.',
        },
        buyer: {
            first_chip_title: 'Фильтры',
            first_chip_desc: 'Хотите подобрать себе того песчаного каракала? Сможете.',
            second_chip_title: 'Продавцы',
            second_chip_desc: 'Мы проверяем наших продавцов. Они появляются на платформе после проверки врачами-ветиринарами',
            third_chip_title: 'Коммуникация',
            third_chip_desc: 'Вам не отвечают или нагрубили? Напишите в саппорт, мы позаботимся о вас.',
            fourth_chip_title: 'Сделки с гарантом',
            fourth_chip_desc: 'Вы не переводите деньги напрямую продавцу, в сделках Pet4U выступает гарантом.',
        },
    }
    const actorKeys = useMemo<typeof actorDict['buyer']>(() => actorDict[actor], [actor])
    return <div className={'flex flex-col gap-20 px-4 md:px-60 mb-24'}>
        <h1 className={'font-montserrat text-2xl font-bold text-left'}>Как это работает</h1>
        <div className={'grid max-sm:grid-cols-1 max-sm:justify-items-center md:grid-cols-4 gap-2 md:gap-5'}>
        <VerticalChip actor={actor} icon={Icon.mobile()} title={actorKeys.first_chip_title} text={actorKeys.first_chip_desc}/>
        <VerticalChip actor={actor} icon={Icon.choices()} title={actorKeys.second_chip_title} text={actorKeys.second_chip_desc}/>
        <VerticalChip actor={actor} icon={Icon.interactions()} title={actorKeys.third_chip_title} text={actorKeys.third_chip_desc}/>
        <VerticalChip actor={actor} icon={Icon.partnership()} title={actorKeys.fourth_chip_title} text={actorKeys.fourth_chip_desc}/>
    </div></div>
}