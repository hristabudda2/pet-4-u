


export const VerticalChip = ({icon, title, text, actor}) => {
    const bgColor = actor === 'seller'? 'bg-brand-blue-100': 'bg-brand-orange-100'
    return <div className={'flex flex-col gap-5'}>
        <div className={`rounded-full ${bgColor} max-w-32 min-h-32 justify-center flex items-center`}>{icon}</div>
        <div className={'text-lg font-bold font-montserrat'}>{title}</div>
        <div className={'text-brand-gray-100 text-lg font-jost max-sm:w-[300px]'}>{text}</div>
    </div>
}