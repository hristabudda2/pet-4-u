'use client'
import Image from 'next/image';
import headerBg from '@public/header-bg.svg'
import logo from '@public/logo.png'
import Link from 'next/link';
import { useState } from 'react';

export const Header = () => {
    return <><div className={'max-md:hidden flex flex-col relative bg-white w-full'}>
        <Image className={'absolute z-0 top-[25px] w-full'} src={headerBg} alt={'aboba'}/>
        <div className={'grid grid-cols-[1fr_0.3fr_1fr] z-10 justify-items-center pt-[20px]'}>
            <div className={'flex flex-row gap-10'}>
                <NavLink href={'/about'}>Обо мне</NavLink>
                <NavLink href={'/search'}>Поиск питомцев</NavLink>
                <NavLink href={'/community'}>Статьи</NavLink>
                <NavLink href={'/contact-us'}>Контакты</NavLink>
            </div>
            <div className={'relative w-[103px]'}><Link href={'/landing/buyer'}><Image className={'top-[-15px] absolute left-0'} height={87} src={logo} alt={'logo'}/></Link></div>
            <div className={'flex flex-row gap-10'}>
                <NavLink href={'/landing/buyer'}>Для покупателей</NavLink>
                <NavLink href={'/landing/seller'}>Для питомников</NavLink>

            </div>
        </div>

    </div>
    <MobileHeader/>
    </>
}

const NavLink = ({children, href}) => <Link href={href} className={'text-lg text-brand-black font-jost'}>{children}</Link>

const BurgerButton = ({toggleOpen, isOpen}: {toggleOpen: () => void, isOpen: boolean}) => {
    const genericHamburgerLine = `h-1 w-6 my-1 rounded-full bg-black transition ease transform duration-300`;

    return (
      <button
        className="flex flex-col h-12 w-12 rounded justify-center items-center group z-50 relative"
        onClick={toggleOpen}
      >
          <div
            className={`${genericHamburgerLine} ${
              isOpen
                ? "rotate-45 translate-y-3 opacity-50 group-hover:opacity-100"
                : "opacity-50 group-hover:opacity-100"
            }`}
          />
          <div
            className={`${genericHamburgerLine} ${
              isOpen ? "opacity-0" : "opacity-50 group-hover:opacity-100"
            }`}
          />
          <div
            className={`${genericHamburgerLine} ${
              isOpen
                ? "-rotate-45 -translate-y-3 opacity-50 group-hover:opacity-100"
                : "opacity-50 group-hover:opacity-100"
            }`}
          />
      </button>
    );
}

const MobileHeader = () => {
    const [isOpen, setIsOpen] = useState(false);
    const close = () => setIsOpen(false)
    return (
      <div className={'bg-brand-bg pl-2 pt-2 md:hidden'}>
     <BurgerButton toggleOpen={() => setIsOpen(prev => !prev)} isOpen={isOpen}/>
          {isOpen && <div className={'animate-slide-down absolute z-40 w-full h-full top-0 left-0 bg-white flex flex-col justify-center items-center gap-5'}>
              <Link onClick={close} href={'/about'} className={'text-lg text-brand-black font-jost'}>Обо мне</Link>
              <Link onClick={close} href={'/search'} className={'text-lg text-brand-black font-jost'}>Поиск питомцев</Link>
              <Link onClick={close} href={'/landing/buyer'}><Image className={''} height={87} src={logo} alt={'logo'}/></Link>
              <Link onClick={close} href={'/community'} className={'text-lg text-brand-black font-jost'}>Статьи</Link>
              <Link onClick={close} href={'/contact-us'} className={'text-lg text-brand-black font-jost'}>Контакты</Link>
          </div>}
      </div>
    );
}