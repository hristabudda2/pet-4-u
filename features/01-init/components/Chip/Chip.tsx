import { Icon } from "../../../../components/Icon/Icon"



export const Chip = ({title, description, actor}) => {
    const bgColor = actor === 'seller'? 'bg-brand-orange-50': 'bg-brand-blue-50'
    const accentColor = actor === 'seller'? 'text-brand-orange-100': 'text-brand-blue-100'
    return <div className={`flex gap-6 p-7 ${bgColor} rounded-[50px]`}>
        <Icon.okay actor={actor}/>
        <div className={'flex flex-col gap-1.5 text-left'}>
            <h2 className={`${accentColor}`}>{title}</h2>
            <span>{description}</span>
        </div>
    </div>
}