import { Icon } from "@components/Icon/Icon";
import Link from "next/link";
import {FC, PropsWithChildren} from "react";

export const Footer = () => {
    return <footer className={'flex flex-col bg-brand-blue-400 lg:px-60 max-md:px-5 pt-8'}>
        <div className={'lg:flex lg:flex-row max-md:grid max-md:grid-cols-2 justify-between mb-16'}>
            <GroupWrapper className={'mb-5'}>
                <GroupHeader>О Pet4U</GroupHeader>
                <GroupItem>О нас</GroupItem>
                <GroupItem>Контакты</GroupItem>
                <GroupItem>Помощь</GroupItem>
                <GroupItem>Отзывы</GroupItem>
            </GroupWrapper>
            <GroupWrapper className={'max-sm:text-end'}>
                <GroupHeader>Сервис</GroupHeader>
                <GroupItem>Для покупателей</GroupItem>
                <GroupItem>Для продавцов</GroupItem>
            </GroupWrapper>
            <GroupWrapper>
                <GroupHeader>Дополнительно</GroupHeader>
                <GroupItem>Инструкции</GroupItem>
                <GroupItem>Полезные статьи</GroupItem>
                <GroupItem>Новости</GroupItem>
            </GroupWrapper>
            <GroupWrapper className={'text-end'}>
                <GroupHeader>Контакты</GroupHeader>
                <Link href={'tel:89059301437'}><Phone>+7 905 930-14-37</Phone></Link>
                <div className={'flex flex-row justify-end gap-7'}>
                    <Link href={''}><Icon.vk/></Link>
                    <Link href={'https://telegram.me/q6int'}><Icon.tg/></Link>
                </div>
            </GroupWrapper>
        </div>
        <div className={'w-full bg-brand-gray-100 h-[1px] mb-5'}/>
        <div className={'text-brand-gray-100 font-jost text-sm text-center mb-5'}>© 2024  PET4U Все права зарегистрированы</div>
    </footer>
}

const GroupWrapper: FC<PropsWithChildren<{className?: string}>> = ({children, className}) => <div className={`flex flex-col gap-5 ${className}`}>{children}</div>
const GroupHeader = ({children}) => <h1 className={'text-white font-bold font-montserrat text-lg'}>{children}</h1>
const GroupItem = ({children}) => <h3 className={'text-brand-gray-100 text-sm font-jost'}>{children}</h3>
const Phone = ({children}) => <span className={'font-jost text-lg text-white'}>{children}</span>