'use client'
import {Input} from "../../../components/Common/Input";
import {Button} from "../../../components/Button";
import deviceTest from '@public/device-test.png';
import Image from "next/image";
import Link from "next/link";

export const ParticipateInTests = () => {
    return <div className={'px-4 md:px-60 mb-24'}><div className={'relative grid max-sm:grid-cols-1 md:grid-cols-[1fr_0.7fr] bg-brand-blue-50 py-12 max-sm:px-5 md:px-24 rounded-[50px]'}>
        <div className={'flex flex-col'}>
            <div className={'font-bold font-montserrat text-2xl md:text-5xl mb-5'}>Примите участие в тестировании</div>
            <div className={'text-brand-gray-100 text-lg mb-7'}>Присоединяйтесь к нашей программе тестирования и помогите сделать наше приложение еще лучше</div>
            <div className={'flex flex-row gap-5 max-sm:justify-center'}>
                <Link href={'/signup'}><Button className={'self-start'} actor={'seller'}>Регистрация</Button></Link>
            </div>
        </div>
        <Image className={'hidden md:block absolute right-[-210px] top-[-70px]'} src={deviceTest} width={700} alt={''}/>
    </div></div>
}