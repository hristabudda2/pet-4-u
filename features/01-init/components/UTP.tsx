import {Button} from "../../../components/Button";
import Image from "next/image";
import landing from '@public/landing.png'
import parse from 'html-react-parser';
import Link from "next/link";

export const UTP = ({actor}) => {
    const bgColor = actor === 'seller' ? 'bg-brand-orange-100' : 'bg-brand-blue-100'
    const buttonActor = actor === 'seller' ? 'buyer' : 'seller';
    const dictKeys = {
        seller: {utp: 'Трудно найти покупателей, которым<br/> не все равно?', sub_utp: 'Публикуйте объявления и находите покупателей, готовых заботиться о ваших животных', actor_button: 'Регистрация', outline_actor_button: 'Найти питомца'},
        buyer: {utp: 'Трудно найти питомник,<br/> идущий на контакт?', sub_utp: 'Находите объявления и питомники, готовые общаться и продавать своих животных', actor_button: 'Регистрация', outline_actor_button: 'Найти питомца'},
    }
    return <div className={`flex flex-col pt-20 ${bgColor} items-center text-center`}>
        <h1 className={'mb-5 text-3xl font-montserrat text-white'}>{parse(dictKeys[actor].utp)}</h1>
        <h4 className={`mb-10 font-jost text-white`}>{parse(dictKeys[actor].sub_utp)}</h4>
        <div className={'flex flex-row gap-[21px] mb-5'}>
            <Link href={'/signup'}><Button actor={buttonActor}>{dictKeys[actor].actor_button}</Button></Link>
            <Link href={'/search'}><Button variant={'outlined'}>{dictKeys[actor].outline_actor_button}</Button></Link>
        </div>
        <Image src={landing} width={954} alt={'selling image'}/>
    </div>
}
