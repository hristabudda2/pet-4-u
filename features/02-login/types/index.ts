

export type indexOfArray = number;
export type Step = { description: string };
export type Incrementer = () => void;
export type EditableInputHandlerFN<T, R = any> = (value: T) => R;
export type DataStepForm = {
    name: string;
    phone: string;
    email: string;
    password: string;
} & Partial<{
    avatarId: string;
}>