import {FC, useEffect, useRef, useState} from "react";
import {FormInput} from "@features/02-login/components/FormInput";
import { Icon } from "@components/Icon/Icon";
import {flushSync} from "react-dom";


export const EditableInput: FC<{defaultValue: any}> = ({defaultValue}) => {
    const [isEditing, setIsEditing] = useState(false);
    const ref = useRef<HTMLInputElement>(null)
    const [value, setValue] = useState(defaultValue);

    useEffect(() => {
        ref.current?.focus()
    }, [ref, isEditing])

    if (!isEditing)
        return <div className={'relative'}>
            <FormInput value={value} className={'bg-brand-blue-50'} disabled={true}/>
            <Icon.edit onClick={() => {
                    setIsEditing(true);
            }} className={'cursor-pointer absolute top-[10px] right-[10px]'}/>
    </div>
    if (isEditing)
        return <FormInput onBlur={() => setIsEditing(false)} ref={ref} value={value} onChange={(e) => setValue(e.target.value)}/>
}