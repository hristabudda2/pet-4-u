'use client'
import {Actor} from "@features/01-init/types";
import buyer from '@public/buyer.png'
import seller from '@public/seller.png'
import {FC, useMemo, useState} from "react";
import {useAccentColor} from "@/hooks/useAccentColor";
import Image from "next/image";
import {useActorTexts} from "@/hooks/useActorTexts";
import {Nullable} from "../../../utils/types";
import {Button} from "@components/Button";
import {useRouter} from "next/navigation";

interface ActorSelectorProps {
    actor: Nullable<Actor>
}


export const ActorSelector: FC<ActorSelectorProps> = ({actor}) => {
    const [actorSelected, setActorSelected] = useState(actor);
    const router = useRouter()

    return <><div className={'flex flex-row justify-center gap-5 mb-14'}>
        <ActorChip select={() => setActorSelected('buyer')} actor={'buyer'} selected={actorSelected === 'buyer'}/>
        <ActorChip select={() => setActorSelected('seller')} actor={'seller'} selected={actorSelected === 'seller'}/>
    </div>
        <Button className={'mb-7'} actor={actorSelected || 'seller'} onClick={() => router.push(`/signup/process?actor=${actorSelected}`)}>Продолжить</Button>
        <Button className={'mb-14'} actor={actorSelected || 'seller'} variant={'plainText'}>Отменить регистрацию</Button>
    </>
}

const ActorChip: FC<{actor: Actor, selected: boolean, select: () => void}> = ({actor, selected, select}) => {
    const actorImages = {
        seller, buyer
    };
    const actorTextsDict = {
        buyer: {
            title: 'Ищу питомца',
            description: 'Найдите своего идеального друга на Pet4U среди большого количества обьявлений от провереных продавцов',
        },
        seller: {
            title: 'Продаю питомца',
            description: 'Разместите своего питомца на нашей платформе и найдите ему заботливый хозяев и уютный дом',
        },
    }
    const actorImage = useMemo(() => actorImages[actor], [actor]);
    const imageWidth = {
        seller: 123,
        buyer: 165,
    }
    const {title, description} = useActorTexts({dictionary: actorTextsDict, actor})
    const {text, border} = useAccentColor({actor});

    return <div onMouseEnter={() => select()} className={`max-w-80 cursor-pointer rounded-[50px] border-4 ${!selected && 'border-transparent'} ${selected && border} flex flex-col items-center px-8 pb-14 pt-8 shadow-lg`}>
        <h1 className={`${text} font-montserrat font-bold text-lg mb-5`}>{title}</h1>
        <h3 className={'font-jost font-bold text-sm text-brand-gray-100 mb-8'}>{description}</h3>
        <Image width={imageWidth[actor]} src={actorImage} alt={'actor image'}/>
    </div>
}