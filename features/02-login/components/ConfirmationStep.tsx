import {Input} from "@components/Common/Input";
import {Button} from "@components/Button";
import {EditableInput} from "@features/02-login/components/EditableInput";


export const ConfirmationStep = () => {
    return <div className={'flex flex-col items-center'}>
        <h1 className={'font-montserrat font-bold text-brand-black text-2xl mb-7'}>2. Подтверждение</h1>
        <h3 className={'font-jost text-lg text-brand-black mb-11'}>Подтвердите свой номер телефона</h3>
        <EditableInput defaultValue={''}/>
        <Button className={'mt-7'}>Отправить код</Button>

    </div>
}

