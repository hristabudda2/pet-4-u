import {FC, PropsWithChildren} from "react";


export const WithLabel: FC<PropsWithChildren<{label: string, className?: string}>> = ({label, className, children}) => {
    return <label className={`flex flex-col gap-2.5 ${className}`}>
        <span className={'text-sm font-montserrat font-bold'}>{label}</span>
        <div>{children}</div>
    </label>
}