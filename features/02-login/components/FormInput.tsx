'use client'

import React, {FC, forwardRef} from 'react';

export const FormInput: FC<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>> = forwardRef((props, ref) => {
    const {placeholder, className, id, ...sanitizedProps} = props

    return (
        <input ref={ref} className={`font-montserrat font-bold text-sm border text-brand-black border-brand-gray-50 rounded-[10px] py-2.5 px-3.5 placeholder:tex ${className}`} {...sanitizedProps} />
    )
});