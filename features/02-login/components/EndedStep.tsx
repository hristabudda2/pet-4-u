




export const EndedStep = () => {
    return <div className={'flex flex-col items-center text-center'}>
        <h1 className={'font-montserrat font-bold text-2xl text-brand-black mb-7'}>Вы зарегистрировались!</h1>
        <h3 className={'font-jost text-lg text-brand-black mb-12'}>Сейчас мы еще работаем над платформой.<br/> Мы с вами свяжемся сразу же, как только закончим технические работы :)</h3>
    </div>
}