import { Icon } from "@components/Icon/Icon";
import {FC} from "react";
import {indexOfArray, Step} from "@features/02-login/types";



interface StepProgressProps {
    steps: Array<Step>;
    currentStep: indexOfArray;
}



export const StepProgressBar: FC<StepProgressProps> = ({steps, currentStep}) => {
    return <div className={'flex flex-row mb-12 justify-between py-4 px-7 w-full rounded-[30px] bg-brand-blue-50'}>{steps.map(mapStepsToComponents(currentStep))}</div>;
}

const EndedStep: FC<Step> = ({description}) => <div className={'flex flex-row items-center gap-2'}>
    <Icon.okayPadded fill={'#F5732A'}/>
    <span className={'font-montserrat font-bold text-sm text-brand-gray-100'}>
        {description}
    </span>
</div>

const CurrentStep: FC<Step & { index: number }> = ({description, index}) => <div className={'flex flex-row items-center gap-2'}>
    <div className={'bg-brand-blue-100 w-6 h-6 rounded-full font-montserrat text-brand-blue-50 text-center'}>{index + 1}</div>
    <span className={'font-montserrat font-bold text-sm text-brand-gray-100'}>
        {description}
    </span>
</div>

const FutureStep: FC<Step & { index: number }> = ({index, description}) => <div className={'flex flex-row items-center gap-2'}>
    <div className={'bg-brand-gray-50 w-6 h-6 rounded-full font-montserrat text-brand-blue-50 text-center'}>{index + 1}</div>
    <span className={'font-montserrat font-bold text-sm text-brand-gray-50'}>
        {description}
    </span>
</div>


const mapStepsToComponents = (currentStep: indexOfArray) => (step: Step, index: indexOfArray) => {
    if (index > currentStep)
        return <FutureStep key={index} {...step} index={index}/>
    if (index < currentStep)
        return <EndedStep key={index} {...step}/>
    if (index === currentStep)
        return <CurrentStep key={index} {...step} index={index}/>
    return null
}