import {DataStepForm, Incrementer} from "@features/02-login/types";
import {useForm} from "react-hook-form";
import {WithLabel} from "@features/02-login/components/WithLabel";
import {Input} from "@components/Common/Input";
import {FC} from "react";
import {Button} from "@components/Button";
import {FormInput} from "@features/02-login/components/FormInput";
import {createUser} from "@features/02-login/data/createUser";
import {signIn} from 'next-auth/react';


interface DataStepProps {
    nextStep: Incrementer;
}

export const DataStep: FC<DataStepProps> = ({nextStep}) => {
    const {register, handleSubmit} = useForm<DataStepForm>()
    return (
        <form className={'flex flex-col items-center'} onSubmit={handleSubmit(async data => {
            await createUser(data)
            await signIn('email', {email: data.email, callbackUrl: '/search'})
        })}>
            <h1 className={'font-montserrat font-bold text-2xl text-brand-black mb-7'}>1. Личные данные</h1>
            <h3 className={'font-jost text-lg text-brand-black mb-12'}>Укажите ваше имя, адрес электронной почты и
                пароль для создания учетной записи</h3>
            <div className={'grid grid-cols-1 gap-x-5 gap-y-7 mb-12'}>
                <WithLabel label={'Имя'}>
                    <FormInput{...register('name', {required: true})}/>
                </WithLabel>
                <WithLabel label={'Email'}>
                    <FormInput type={'email'} {...register('email', {required: true})}/>
                </WithLabel>
            </div>
            <Button className={'mb-7'} type={'submit'}>Сохранить и продолжить</Button>
            <Button className={'mb-24'} variant={'plainText'}>Отменить регистрацию</Button>
        </form>
    );
}

