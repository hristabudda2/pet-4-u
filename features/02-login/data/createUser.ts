'use server';


import {Prisma} from "@prisma-client";
import {master} from "@managers/PrismaManager";

export async function createUser(data: Prisma.userUncheckedCreateInput) {
    return master.user.create({data});
}