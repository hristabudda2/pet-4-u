module.exports = {
    apps: [
        {
            name: "pet4u",
            script: "node_modules/next/dist/bin/next",
            args: "start -p 85",
            watch: false,
        },
    ],
};