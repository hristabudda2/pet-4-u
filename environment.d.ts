declare global {
    namespace NodeJS {
        interface ProcessEnv {
            NEXT_PUBLIC_COUNTER_ID: string
            NEXT_PUBLIC_HOST: string
            ACCESS_KEY: string;
            SECRET_KEY: string;
            S3_STORAGE_LINK: string;
            NEXT_PUBLIC_SEARCH_PAGE_SIZE: string;
            NEXT_AUTH_SECRET: string
            EMAIL_FROM: string;
            EMAIL_HOST: string;
            EMAIL_PORT: string;
        }
    }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export { };