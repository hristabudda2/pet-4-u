'use client';
import {YandexMetricaProvider} from "next-yandex-metrica"


export const Providers = ({children}) => {
    if (process.env.NEXT_PUBLIC_HOST === 'localhost')
        return children
    return <YandexMetricaProvider initParameters={{clickmap: true, trackLinks: true, accurateTrackBounce: true}}
                                  tagID={Number(process.env.NEXT_PUBLIC_COUNTER_ID)}>
        {children}
    </YandexMetricaProvider>

}