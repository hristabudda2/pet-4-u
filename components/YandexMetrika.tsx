'use client'

import { useEffect } from 'react'
import { usePathname, useSearchParams } from 'next/navigation'
import {ym} from "next-yandex-metrica";

export default function YandexMetrika() {
    const pathname = usePathname()
    const searchParams = useSearchParams()

    useEffect(() => {
        const url = `${pathname}?${searchParams}`
        ym(Number(process.env.NEXT_PUBLIC_COUNTER_ID), 'hit', url);
    }, [pathname, searchParams])

    return null
}