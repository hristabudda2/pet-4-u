'use client';
import { FC } from 'react';
import { DictionaryKey } from '@specs/helpers';

type ButtonTypes = 'default' | 'outlined' | 'plainText';
interface ButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    actor?: 'seller' | 'buyer';
    variant?: ButtonTypes;
    textClassName?: string;
}

export const Button: FC<ButtonProps> = ({actor, variant = 'default', className, textClassName, ...props}) => {
    const buttonColor = actor === 'seller'? 'bg-brand-orange-100': 'bg-brand-blue-100';
    const isNeedToApplyColor =  !variant || variant === 'default';
    const variantStyles: {[key in ButtonTypes]: string} = {
        default: 'rounded-[30px] py-2.5 px-12 ',
        outlined: 'rounded-[30px] py-2.5 px-6 border-white border',
        plainText: '!text-brand-blue-100',
    }
    const textColor: {[key in ButtonTypes]: string} = {
        default: 'text-white',
        outlined: 'text-white',
        plainText: 'text-brand-blue-100',
    }
    return (<button
        {...props}
        className={`${isNeedToApplyColor && buttonColor}  ${variantStyles[variant]} ${className}`}>
  <span
      className={`${textColor[variant]} font-jost text-lg ${textClassName}`}>
      {props.children}
  </span>
    </button>);
};

