'use client'
import classes from './Input.module.scss'

import React, { FC } from 'react';
import { nanoid } from 'nanoid';

export const Input: FC<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>> = (props, context) => {
    const {placeholder, id, ...sanitizedProps} = props
    const enforcedId = id? id: nanoid(3);
    const dummyPlaceholder = ' '
    return (
        <div className={classes.field}>
        <input placeholder={dummyPlaceholder} id={enforcedId} {...sanitizedProps} />
            <label htmlFor={enforcedId} title={placeholder} data-title={placeholder}></label>
    </div>
    )
}