import React, { FC, ReactNode, useEffect, useRef } from 'react';
import { useOnClickOutside } from '@hooks/useOnClickOutside';
import {getBoundingClientObj} from 'react-select/dist/declarations/src/utils';

interface PopupProps {
  close: () => void
  isOpen: boolean;
}


type PropsWithChildren<P = unknown> = P & { children: ReactNode };

const Popup: FC<PropsWithChildren<PopupProps>> = ({children, isOpen, close}) => {
  const popupRef = useRef<HTMLDivElement>(null)
  useOnClickOutside(popupRef, close)
  return (
    <>
      {isOpen && (
        <div style={{top: window.scrollY}} className={'absolute justify-center items-center flex w-full h-[100vh] bg-brand-gray-100 left-0 z-30 bg-opacity-80'}>
          <div className={''}>
            <div
              ref={popupRef}
              className={'mx-auto bg-white z-40 py-16 px-5 rounded-[30px]'}>
              {children}
            </div>
          </div>
        </div>
      )}
    </>
  )
}
export default Popup
