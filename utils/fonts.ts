
import localFont from 'next/font/local'

export const montserratAlternates = localFont({
    src: [
        {
            path: '../public/Montserrat_Alternates/MontserratAlternates-Regular.ttf',
            weight: '400',
            style: 'normal'
        },{
            path: '../public/Montserrat_Alternates/MontserratAlternates-Italic.ttf',
            weight: '400',
            style: 'italic'
        },
        {
            path: '../public/Montserrat_Alternates/MontserratAlternates-Bold.ttf',
            weight: '700',
            style: 'normal'
        }
    ],
    adjustFontFallback: 'Arial',
    preload: true,
    display: 'swap',
    variable: '--font-montserrat-alternates'
})

export const jost = localFont({
    adjustFontFallback: 'Arial',
    src: [
        {
            path: '../public/Jost/Jost-Regular.ttf',
            weight: '400',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-Black.ttf',
            weight: '900',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-BlackItalic.ttf',
            weight: '900',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-Italic.ttf',
            weight: '400',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-Bold.ttf',
            weight: '700',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-BoldItalic.ttf',
            weight: '700',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-ExtraBold.ttf',
            weight: '900',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-ExtraBoldItalic.ttf',
            weight: '900',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-Medium.ttf',
            weight: '500',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-MediumItalic.ttf',
            weight: '500',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-SemiBold.ttf',
            weight: '600',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-SemiBoldItalic.ttf',
            weight: '600',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-Thin.ttf',
            weight: '300',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-ThinItalic.ttf',
            weight: '300',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-Light.ttf',
            weight: '200',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-LightItalic.ttf',
            weight: '200',
            style: 'italic'
        },
        {
            path: '../public/Jost/Jost-ExtraLight.ttf',
            weight: '100',
            style: 'normal'
        },
        {
            path: '../public/Jost/Jost-ExtraLightItalic.ttf',
            weight: '100',
            style: 'italic'
        }

    ],
    variable: '--font-jost'
})
